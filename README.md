# Poodle - e-Learning Platform

The Poodle is a project that aims to develop an e-learning platform for students and teachers. It is a robust web application that offers extensive functionality for user management, course administration, and section management. It caters to different user roles, including teachers, students, and administrators. The application provides endpoints for user registration, login, profile management, and access control. It enables teachers to create and manage courses, while students can subscribe to courses, rate them, and track their progress. Administrators have access to endpoints for course and student management. The project also includes features such as tagging and pagination for enhanced organization and scalability. 

##### Technologies used: Python, FAST API, MariaDB, JWT, OAuth 2.0

## The Team:
Donika TODOROVA, Ema KUNCHEVA, Stanislav ANTONOV 

## Getting started:

To properly set up the virtual environment (venv) and install the required dependencies for running the API, follow these steps:
<details>
<summary>Click here for more details</summary>

1. Set up the virtual environment:
- Open your command prompt or terminal.
- Execute the following command to create a virtual environment named "venv":
**py -3 -m venv venv**

This will create a new directory named "venv" that will contain the virtual environment files.

2. Activate the virtual environment:
- For PowerShell (pwsh) activation:
    - Execute the following command to activate the virtual
 environment:
**.\venv\Scripts\Activate.ps1**

- For Command Prompt (cmd) activation:
    - Execute the following command to activate the virtual environment:
**.\venv\Scripts\activate.bat**

After activation, you will notice that the command prompt or terminal prompt will change to indicate that you are now working within the virtual environment.

3. Install the required dependencies:
- Make sure you are still within the virtual environment (activated as mentioned in step 2).
- Execute the following command to install the dependencies listed in the "**requirements.txt**" file:
**pip install -r requirements.txt**

With these steps completed, you should have the virtual environment set up and all the required dependencies installed. You are now ready to run the API.
</details>

## Database:
The Poodle e-Learning Platform uses a relational database to store its data. The database design follows a normalized structure to avoid data duplication and ensure data integrity. The core domain objects and their relationships have been modeled accordingly. The SQL syntax used in this project is specific to MariaDB.
<details>
<summary>Click here for more details</summary>
![Database Schema](Poodle_E-learn/schema_db.PNG)
</details>

## Functionality
The system consists of three main entities: Users (students, teachers and admins), Courses (public or premium), and Sections and is divided into 5 (five) routers:


<details>
<summary>Users(Teachers/Students):</summary>
Contains 6 (six) endpoints related to user management:<br>
<br>

1. **'/users/students/register'** (POST): This endpoint is used to register a new student. It expects a request body containing student registration data. If the user with the given email already exists, it returns a 400 response with an error message. Otherwise, it creates a new student and returns a 201 response with the student's information.

2. **'/users/teachers/register'** (POST): This endpoint is used to register a new teacher. It follows a similar pattern as the register_student endpoint but for teachers. It creates a new teacher if the user doesn't already exist and returns a 201 response with the teacher's information.

3. **'/users/login'** (POST): This endpoint handles user login. It expects an OAuth2 password request form containing the user's credentials. If the user doesn't exist, it returns a 404 response. If the login data is invalid, it returns a 400 response. Otherwise, it generates an access token for the user and returns a 200 response with the access token.

4. **'/users/'** (GET): This endpoint retrieves the profile information of a user. It expects a valid access token in the request header. If the user exists and the access token is valid, it returns a 200 response with the user's profile information. If the user is an admin, it returns a 403 response with an error message.

5. **'/users/students'** (PUT): This endpoint allows editing the profile of a student. It expects a request body containing updated student data and a valid access token. It updates the student's information if the user is a student and the data is valid, and returns a 200 response with the updated student's information. Otherwise, it returns a 400 response with an error message.

6. **'/users/teachers'** (PUT): This endpoint is similar to the edit_student_account endpoint but for teachers. It allows editing the profile of a teacher. It follows a similar pattern and returns a 200 response with the updated teacher's information if the user is a teacher and the data is valid.
</details>

<details><summary>Admins</summary>
Contains 5 (five) endpoints designed for admin-related operations: <br>
<br>

1. **'/admin/courses'** (GET): Retrieves all courses and the number of students in each course. It supports pagination, sorting, and searching.

2. **'/admin/courses/{course_id}/ratings'** (GET): Retrieves the students who have rated a specific course identified by course_id. It supports pagination and sorting. Returns an error if the course doesn't exist.

3. **'/admin/courses/{course_id}/status'** (PUT): Changes the status (deactivates/activates) of a specific course identified by course_id. Returns an error if the course doesn't exist or if the course has students enrolled.

4. **'/admin/students/{user_id}/status'** (PUT): Changes the status (deactivates/activates) of a student's account identified by user_id. Returns an error if the user doesn't exist or if the user is not a student.

5. **'/admin/courses/{course_id}/students/{user_id}'** (DELETE): Deletes a student from a specific course identified by course_id and user_id. Returns an error if the user or student doesn't exist or if the course doesn't exist, or if the user is not subscribed or pending in the course.
</details>

<details><summary>Courses</summary>

Contains 9 (nine) endpoint with different functionalities related to course management, user roles, and interactions.
<br>
1. **'/courses/public'** : Retrieves course information for all public and active courses, accessible to unregistered users. It supports pagination, sorting, searching, and filtering by tags.

2. **'/courses/'** (GET): Retrieves detailed course information for all active courses, accessible to registered users. It supports pagination, sorting, searching, and filtering by tags.

3. **'/courses/subscribed'** (GET): Retrieves detailed course information for courses that a registered user is subscribed to. It supports pagination, sorting, searching, and filtering by tags.

4. **'/courses/{course_id}/unsubscribe'** (PUT): Allows a student to unsubscribe from a private course they are currently subscribed to.

5. **'/courses/{course_id}/subscribe'** (PUT): Allows a student to subscribe to a course, with restrictions on the maximum number of premium courses they can subscribe to.

6. **'/courses/'** (POST): Allows a teacher to create a new course.

7. **'/courses/{course_id}/enroll/{student_id}'** (PUT): Allows a teacher to approve enrollment requests sent by students for their course.

8. **'/courses/{course_id}'** (POST): Allows a student to rate a course they are subscribed to.

9. **'/courses/{course_id}/tags'** (POST): Allows a teacher to assign tags to an existing course.
</details>


<details><summary>Sections</summary>

Contains 6 (six) endpoint with different functionalities related to the management of course sections.
<br>
1. **'/courses/{course_id}/sections/progress'** (GET): This endpoint is used to track the progress of a student in a specific course. It returns the percentage of sections visited by the student. Only subscribed students can access this endpoint.

2. **'/courses/{course_id}/sections/'** (GET): This endpoint retrieves all the sections of a course along with detailed course information. It is accessible to both registered students and teachers. Students need to be subscribed to the course to access this endpoint, while teachers need to own the course. The endpoint supports pagination, sorting, searching, and filtering by tags.

3. **'/courses/{course_id}/sections/'** (POST): This endpoint allows teachers to create a new section for a course they own. Only the owner of the course can create a section. The endpoint expects the course ID and the section details as input and returns the created section.

4. **'/courses/{course_id}/sections/{section_id}'** (PUT): This endpoint is used to update an existing section in a course. Only the owner of the course can modify the section. The endpoint expects the course ID, section ID, and the updated section details as input. It returns the updated section.

5. **'/courses/{course_id}/sections/{section_id}'** (DELETE): This endpoint allows the owner of a course to delete a section from the course. It requires the course ID and the section ID as input and returns a successful response with a status code of 204 (No Content).

6. **'/courses/{course_id}/sections/{section_id}/visited'** (POST): This endpoint is used by subscribed students to mark a section as visited. It requires the course ID and section ID as input and returns a successful response with a status code of 201 (Created).
</details>

<details><summary>Tags</summary>

Contains 1 (one) endpoint handling tags functionality.
<br>
1. **'/tags/'** (GET): This endpoint retrieves all available tags. It is accessible to teachers only. The endpoint retrieves the total number of tags available and calculates the offset based on the page and page size parameters. It also supports pagination. 
</details>


## Swagger Documentation
Explore the capabilities of our API by accessing our Swagger documentation. The Swagger documentation provides a comprehensive overview of our API endpoints, request/response formats, and authentication requirements. To dive into the details, simply click on the API Documentation link. It will take you to the Swagger UI, where you can interactively explore and test our API. 

[Link to Documentation](http://16.170.203.72/docs)

#### Note:
Please note that due to hosting limitations, the link to the documentation may be unavailable (hey, hosting is expensive). However, you can still test the functionality of the API by using the SQL dump file included in the project directory. Simply import the SQL dump file into your database and configure the API accordingly. This way, you can interact with the API and experience its features even if the documentation link is inaccessible by accessing [127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)

<details><summary>Setup .env</summary>
You would also need to create a .env file that should look like this:

DATABASE_USER={}<br>
DATABASE_PASSWORD={}<br>
DATABASE_HOST={}<br>
DATABASE_PORT={}<br>
DATABASE_NAME=mariadbtest<br>
SECRET_KEY={}<br>
ALGORITHM=HS256<br>
ACCESS_TOKEN_EXPIRE_MINUTES={}
</details>


## Technical Specifications

- The project followed coding principles such as KISS, SOLID, and DRY to ensure clean and maintainable code.

- The REST API was designed adhering to best practices, ensuring compliance with RESTful principles for efficient communication and interoperability.

- A tiered project structure was implemented, separating the application into distinct layers to promote modularity and organization.

- Proper exception handling and propagation were implemented throughout the project to handle errors gracefully and provide meaningful feedback to users.

- During development, careful consideration was given to future modifications and maintainability, ensuring that code was designed with flexibility and ease of modification in mind.

## About Us
We are a talented team of three individuals: Doni, Ema, and Stan. As graduates of Telerik Academy, we have embarked on an exciting journey with our final project, showcasing our skills and knowledge in a practical application. Passionate about coding and eager to learn, we have poured our hearts into this project, adhering to industry best practices and leveraging the principles of KISS, SOLID, and DRY in our code. Together, we are dedicated to delivering a remarkable project that reflects our passion for coding and our commitment to continuous learning.
