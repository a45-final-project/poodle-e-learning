from pydantic import BaseModel, constr, Field
from typing import List

class CourseTypes:
    INT_TO_STR = {1: 'public', 2: 'private'}
    STR_TO_INT = {'public': 1, 'private': 2}


class SubscriptionTypes:
    INT_TO_STR = {1: 'active', 2: 'terminated', 3: 'pending'}
    STR_TO_INT = {'active': 1, 'terminated': 2,'pending': 3}


class Section(BaseModel):
    id: int | None
    title: str
    content_type: str
    description: str | None
    info: str | None

    @classmethod
    def query_result(cls, id, title, content_type, description, info):
        return cls(
            id = id,
            title = title,
            content_type = content_type,
            description = description, 
            info = info)
    
    class Config:
        schema_extra = {
        "example": {
                "id": 1,
                "title": "Functions",
                "content_type": "PowerPoint Document",
                "description": "Understand what is a function, how to create and use one.",
                "info": "https://www.w3schools.com/python/python_functions.asp"}}
    

class SectionForStudent(Section):
    visited: bool | None

    @classmethod
    def query_result(cls, id, title, content_type, description, info, visited):
        inherited = Section.query_result(id, title, content_type, description, info).dict()
        return cls(**inherited,
            visited = visited)
    

class SectionUpdate(BaseModel):
    title: str | None
    content_type: str | None
    description: str | None
    info: str | None

    @classmethod
    def query_result(cls, title, content_type, description, info):
        return cls(
            title = title,
            content_type = content_type,
            description = description, 
            info = info)

    class Config:
        schema_extra = {
        "example": {
                "title": "Functions",
                "content_type": "PowerPoint Document",
                "description": "Understand what is a function, how to create and use one.",
                "info": "https://www.w3schools.com/python/python_functions.asp"}}


class CourseCreate(BaseModel):
    id: int | None
    title: str # unique
    description: str
    objectives: str
    tags: str | None 
    type: constr(regex='^public|premium$')


class CourseResponceModelPublic(BaseModel):
    id: int | None
    title: str # unique
    description: str
    tags: str | None 
    rating: float | None

    @classmethod
    def from_query_result(cls, id, title, description, tags, rating):
        return cls(
            id=id,
            title=title,
            description=description,
            tags=tags,
            rating=rating)


class CourseResponceModel(BaseModel):
    id: int | None
    title: str # unique
    description: str
    objectives: str
    type: str
    tags: str | None
    rating: float | None

    @classmethod
    def from_query_result(cls, id, title, description, objectives, type, tags, rating):
        return cls(
            id=id,
            title=title,
            description=description,
            objectives=objectives,
            type=CourseTypes.INT_TO_STR[type],
            tags=tags,
            rating=rating)
    
    class Config:
        schema_extra = {
        "example": {
                "id": 1,
                "title": "Python Basics",
                "description": "Learn the basics of Python programming language.",
                "objectives": "Master Python core in just 30 days.",
                "type": "public",
                "tags": "python, programming, basics",
                "rating": 8}}


class CourseResponceModelAdmin(BaseModel):
    id: int | None
    title: str # unique
    description: str
    objectives: str
    type: str
    tags: str | None
    rating: float | None
    students: int

    @classmethod
    def from_query_result(cls, id, title, description, objectives, type, tags, rating, students):
        return cls(
            id=id,
            title=title,
            description=description,
            objectives=objectives,
            tags=tags,
            rating=rating,
            type=CourseTypes.INT_TO_STR[type],
            students=students)


class CourseWithSections(BaseModel):
    course: CourseResponceModel
    sections: list[SectionForStudent]

    @classmethod
    def query_result(cls, course, sections):
        return cls(
            course = course,
            sections = sections)
    

class CreateTags(BaseModel):
    tags: List[str] = Field(..., unique_items=True)


class StudentInfoForTeacher(BaseModel):
    id:int
    email: str
    deactivated: bool
    subscription: str

    @classmethod
    def query_result(cls, id, email, deactivated, subscription):
        return cls(
            id=id,
            email = email,
            deactivated = deactivated,
            subscription = SubscriptionTypes.INT_TO_STR[subscription])