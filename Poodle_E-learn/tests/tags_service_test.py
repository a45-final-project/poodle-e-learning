import sys
import os
import unittest
from unittest import mock

from services import tags_service

# Add the project root directory to the Python module search path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

class TagsService_Should(unittest.TestCase):
    @mock.patch('services.tags_service.database', autospec=True)
    def test_get_all_tags_returnsListOfTags(self, mock_db):
        # Arrange
        expected_tags = [
            {'id': 1, 'name': 'Tag1'},
            {'id': 2, 'name': 'Tag2'},
            {'id': 3, 'name': 'Tag3'}
        ]
        mock_db.read_query.return_value = [
            {'id': 1, 'name': 'Tag1'},
            {'id': 2, 'name': 'Tag2'},
            {'id': 3, 'name': 'Tag3'}
        ]
        offset = 0
        page_size = 10
        # Act
        result = tags_service.get_all_tags(offset, page_size)
        # Assert
        self.assertEqual(result, expected_tags)

    @mock.patch('services.tags_service.database', autospec=True)
    def test_count_all_tags_returnsCount(self, mock_db):
        # Arrange
        expected_count = 3
        mock_db.query_count.return_value = 3
        # Act
        result = tags_service.count_all_tags()
        # Assert
        self.assertEqual(result, expected_count)

    @mock.patch('services.tags_service.database', autospec=True)
    def test_create_tag_returnsId(self, mock_db):
        # Arrange
        tag_name = 'Tag1'
        expected = 1
        mock_db.insert_query.return_value = 1
        # Act
        result = tags_service.create_tag(tag_name)
        # Assert
        self.assertEqual(result, expected)




