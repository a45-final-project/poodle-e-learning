import sys
import os
import unittest
from unittest import mock
# from mariadb import IntegrityError

# Add the project root directory to the Python module search path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from data.models_users import StudentUpdate, TeacherUpdate, UserDatabase, UserStudentRegister, UserTeacherRegister, StudentDatabase, TeacherDatabase, StudentResponseModel, TeacherResponseModel
from services import users_service

USER_FROM_DATABASE = UserDatabase(
    id=1, 
    email="dictum.augue@icloud.ca", 
    password="6Lw71ll@7CAS", 
    role="admin", 
    deactivated=False)

TEACHER_REGISTER = UserTeacherRegister(
    id=1, 
    email="dictum.augue@icloud.ca", 
    password="6Lw71ll@7CAS", 
    deactivated=False, 
    first_name="Jane", 
    last_name="Doe", 
    phone="123456789", 
    linked_in="https://www.linkedin.com/in/janedoe/")

TEACHER_RESPONSE = TeacherResponseModel(
    email="dictum.augue@icloud.ca", 
    deactivated=False, first_name="Jane", 
    last_name="Doe", 
    phone="123456789", 
    linked_in="https://www.linkedin.com/in/janedoe/")

TEACHER_UPDATE = TeacherUpdate(
    id=1,
    email="dictum.augue@icloud.ca",
    password="6Lw71ll@7CAS",
    deactivated=False,
    first_name="John",
    last_name="Doe", 
    phone="123456789", 
    linked_in="https://www.linkedin.com/in/janedoe/")

TEACHER_RESPONSE_FOR_UPDATE = TeacherResponseModel(
    email="dictum.augue@icloud.ca", 
    deactivated=False, 
    first_name="John", 
    last_name="Doe", 
    phone="123456789", 
    linked_in="https://www.linkedin.com/in/janedoe/")

TEACHER_FROM_DATABASE = TeacherDatabase(
    id=1, 
    email="dictum.augue@icloud.ca", 
    password="6Lw71ll@7CAS", 
    role='teacher', 
    deactivated=False, 
    first_name="Jane", 
    last_name="Doe", 
    phone="123456789", 
    linked_in="https://www.linkedin.com/in/janedoe/")

STUDENT_REGISTER = UserStudentRegister(
    id=1, 
    email="dictum.augue@icloud.ca", 
    password="6Lw71ll@7CAS", 
    deactivated=False, 
    first_name="Jane", 
    last_name="Doe")

STUDENT_RESPONSE = StudentResponseModel(
    email="dictum.augue@icloud.ca", 
    deactivated=False, 
    first_name="Jane", 
    last_name="Doe")

STUDENT_UPDATE = StudentUpdate(
    id=1,
    email="dictum.augue@icloud.ca",
    password="6Lw71ll@7CAS",
    deactivated=False,
    first_name="John",
    last_name="Doe")

STUDENT_RESPONSE_FOR_UPDATE = StudentResponseModel(
    email="dictum.augue@icloud.ca", 
    deactivated=False, 
    first_name="John", 
    last_name="Doe")

STUDENT_FROM_DATABASE = StudentDatabase(
    id=1, 
    email="dictum.augue@icloud.ca", 
    password="6Lw71ll@7CAS", 
    role='student', 
    deactivated=False, 
    first_name="Jane", 
    last_name="Doe")


class UserService_Should(unittest.TestCase):


    def test_create_createsStudentWhenDataIsValid(self):
        # Arrange
        generated_id = 1
        expected = STUDENT_RESPONSE

        with mock.patch('services.users_service.database', autospec=True) as mock_db:
            mock_db.insert_query.return_value = generated_id
            # Act
            result = users_service.create_student(STUDENT_REGISTER)
            # Assert
            self.assertEqual(result, expected)   


    def test_create_createsTeacherWhenDataIsValid(self):
        # Arrange
        generated_id = 1
        expected = STUDENT_RESPONSE

        with mock.patch('services.users_service.database', autospec=True) as mock_db:
            mock_db.insert_query.return_value = generated_id
            # Act
            result = users_service.create_student(STUDENT_REGISTER)
            # Assert
            self.assertEqual(result, expected) 


    @mock.patch('services.users_service.database', autospec=True)
    def test_get_by_email_returnsUserIfUserExist(self, mock_db):
        # Arrange
        email = "dictum.augue@icloud.ca"
        expected = USER_FROM_DATABASE
        mock_db.read_query.return_value = [(1, "dictum.augue@icloud.ca", "6Lw71ll@7CAS", 3, False)]
        #Act
        result = users_service.get_by_email(email)
        # Assert
        self.assertEqual(result, expected)


    @mock.patch('services.users_service.database', autospec=True)
    def test_get_by_email_returnsNoneIfUserNotExist(self, mock_db):
        # Arrange
        email = "dictum.augue@icloud.ca"
        mock_db.read_query.return_value = []
        #Act
        result = users_service.get_by_email(email)
        # Assert
        self.assertIsNone(result)


    @mock.patch('services.users_service.database', autospec=True)
    def test_get_by_id_returnsUserIfUserExist(self, mock_db):
        # Arrange
        id = 1
        expected = USER_FROM_DATABASE
        mock_db.read_query.return_value = [(1, "dictum.augue@icloud.ca", "6Lw71ll@7CAS", 3, False)]
        #Act
        result = users_service.get_by_id(id)
        # Assert
        self.assertEqual(result, expected)

    @mock.patch('services.users_service.database', autospec=True)
    def test_get_by_id_returnsNoneIfUserNotExist(self, mock_db):
        # Arrange
        id = 1
        mock_db.read_query.return_value = []
        #Act
        result = users_service.get_by_id(id)
        # Assert
        self.assertIsNone(result)

    @mock.patch('services.users_service.database', autospec=True)
    def test_get_student_by_id_returnsStudentIfStudentExist(self, mock_db):
        # Arrange
        id = 1
        expected = STUDENT_FROM_DATABASE
        mock_db.read_query.return_value = [(1, "dictum.augue@icloud.ca", "6Lw71ll@7CAS", 1, False, "Jane", "Doe")]
        #Act
        result = users_service.get_student_by_id(id)
        # Assert
        self.assertEqual(result, expected)


    @mock.patch('services.users_service.database', autospec=True)
    def test_get_student_by_id_returnsNoneIfStudentNotExist(self, mock_db):
        # Arrange
        id = 1
        mock_db.read_query.return_value = []
        #Act
        result = users_service.get_student_by_id(id)
        # Assert
        self.assertIsNone(result)

    @mock.patch('services.users_service.database', autospec=True)
    def test_get_teacher_by_id_returnsTeacherIfTeacherExist(self, mock_db):
        # Arrange
        id = 1
        expected = TEACHER_FROM_DATABASE
        mock_db.read_query.return_value = [(1, "dictum.augue@icloud.ca", "6Lw71ll@7CAS", 2, False, "Jane", "Doe", "123456789", "https://www.linkedin.com/in/janedoe/")]
        #Act
        result = users_service.get_teacher_by_id(id)
        # Assert
        self.assertEqual(result, expected)
        

    @mock.patch('services.users_service.database', autospec=True)
    def test_edit_student_returnsStudentIfStudentExist(self, mock_db):
        # Arrange
        id = 1
        mock_db.read_query.return_value = [(1, "dictum.augue@icloud.ca", "6Lw71ll@7CAS", 2, False, "Jane", "Doe")]
        mock_db.update_query.return_value = 1
        expected = STUDENT_RESPONSE_FOR_UPDATE

        #Act
        result = users_service.edit_student(id, STUDENT_UPDATE)
        # Assert
        print(result)
        self.assertEqual(result, expected)


    @mock.patch('services.users_service.database', autospec=True)
    def test_edit_student_returnsNoneIfStudentNotExist(self, mock_db):
        # Arrange
        id = 1
        mock_db.read_query.return_value = []
        mock_db.update_query.return_value = 1

        #Act
        result = users_service.edit_student(id, STUDENT_UPDATE)
        # Assert
        self.assertIsNone(result)


    @mock.patch('services.users_service.database', autospec=True)
    def test_edit_teacher_returnsTeacherIfTeacherExist(self, mock_db):
        # Arrange
        id = 1
        mock_db.read_query.return_value = [(1, "dictum.augue@icloud.ca", "6Lw71ll@7CAS", 2, False, "Jane", "Doe", "123456789", "https://www.linkedin.com/in/janedoe/")]
        mock_db.update_query.return_value = 1
        expected = TEACHER_RESPONSE_FOR_UPDATE

        #Act
        result = users_service.edit_teacher(id, TEACHER_UPDATE)
        # Assert
        print(result)
        self.assertEqual(result, expected)


    @mock.patch('services.users_service.database', autospec=True)
    def test_edit_teacher_returnsNoneIfSTeacherNotExist(self, mock_db):
        # Arrange
        id = 1
        mock_db.read_query.return_value = []
        mock_db.update_query.return_value = 1

        #Act
        result = users_service.edit_student(id, TEACHER_UPDATE)
        # Assert
        self.assertIsNone(result)


    def test_login_returnsUserWhenDataIsValid(self):
    # Arrange

        username = "dictum.augue@icloud.ca"
        password = "6Lw71ll@7CAS"
        user = USER_FROM_DATABASE

        mock_get_by_email = mock.MagicMock(return_value=user)
        mock_verify_password = mock.MagicMock(return_value=True)

        with mock.patch('services.users_service.get_by_email', new=mock_get_by_email):
            with mock.patch('services.users_service._verify_password', new=mock_verify_password):

                # Act
                result = users_service.login(username, password)

                # Assert
                self.assertEqual(result, user)


    def test_login_returnsUserWhenDataIsValid(self):
    # Arrange

        username = "dictum.augue@icloud.ca"
        password = "6Lw71ll@7CAS"
        user = USER_FROM_DATABASE

        mock_get_by_email = mock.MagicMock(return_value=user)
        mock_verify_password = mock.MagicMock(return_value=False)

        with mock.patch('services.users_service.get_by_email', new=mock_get_by_email):
            with mock.patch('services.users_service._verify_password', new=mock_verify_password):

                # Act
                result = users_service.login(username, password)

                # Assert
                self.assertIsNone(result)
    
# End of user_service tests