import sys
import os
import unittest
from unittest import mock

# Add the project root directory to the Python module search path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from data.models import CourseResponceModelPublic, CourseResponceModel
from services import courses_service


COURSES = [
        CourseResponceModelPublic(id=1, title='Course B', description='Description 1', tags='Tag2', rating=4),
        CourseResponceModelPublic(id=2, title='Course C', description='Description 2', tags='Tag1, Tag3', rating=2),
        CourseResponceModelPublic(id=3, title='Course A', description='Description 3', tags=None, rating=3),
    ]

EXPECTED_ASC_TITLE = [
        CourseResponceModelPublic(id=3, title='Course A', description='Description 3', tags=None, rating=3),
        CourseResponceModelPublic(id=1, title='Course B', description='Description 1', tags='Tag2', rating=4),
        CourseResponceModelPublic(id=2, title='Course C', description='Description 2', tags='Tag1, Tag3', rating=2),
    ]

EXPECTED_DESC_TITLE = [
        CourseResponceModelPublic(id=2, title='Course C', description='Description 2', tags='Tag1, Tag3', rating=2),
        CourseResponceModelPublic(id=1, title='Course B', description='Description 1', tags='Tag2', rating=4),
        CourseResponceModelPublic(id=3, title='Course A', description='Description 3', tags=None, rating=3),
    ]

EXPECTED_LOGGED_IN_RETURN = ([
    CourseResponceModel(id=1,title='Course 1',description='Description 1', objectives='Objectives 1', type='public', tags='Tag1, Tag2',rating=4.5),
    CourseResponceModel(id=2,title='Course 2',description='Description 2', objectives='Objectives 2', type='private', tags='Tag4',rating=2.5)
],1)

EXPECTED_STUDENT_RETURN = [
    CourseResponceModel(id=1,title='Course 1',description='Description 1',objectives='Objectives 1',type='public',tags='Tag1, Tag2',rating=4.5),
    CourseResponceModel(id=2,title='Course 2',description='Description 2',objectives='Objectives 2',type='private',tags='Tag4',rating=2.5)
]

EXPECTED_TEACHER_RETURN = [CourseResponceModel(id=3,title='Course 3',description='Description 3',objectives='Objectives 3',type='public',tags='Tag3, Tag4',rating=3.8)
]


EXPECTED_PUBLIC_RETURN = ([
        CourseResponceModelPublic(id=1, title='Course 1', description='Description 1', tags='Tag1, Tag2', rating=4.5),
        CourseResponceModelPublic(id=2, title='Course 2', description='Description 2', tags='Tag2, Tag3', rating=3.8)
],1)

class CoursesService_Should(unittest.TestCase):
    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_all_public(self, mock_db):
        # Arrange
        expected_courses, expected_count = EXPECTED_PUBLIC_RETURN
        mock_db.read_query.return_value = [
            (1, 'Course 1', 'Description 1', 'Tag1, Tag2', 4.5),
            (2, 'Course 2', 'Description 2', 'Tag2, Tag3', 3.8)
        ]
        search = 'Course 2'
        tags = 'Tag1'
        page = 1
        limit = 10
        # Act
        result_courses, result_count = courses_service.get_all_public(page=page, limit=limit, search=search, tags=tags)
        # Assert
        self.assertEqual(result_courses, expected_courses)
        self.assertEqual(result_count, expected_count)




    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_all_public_without_search(self, mock_db):
        # Arrange
        expected_courses, expected_count = EXPECTED_PUBLIC_RETURN
        mock_db.read_query.return_value = [
            (1, 'Course 1', 'Description 1', 'Tag1, Tag2', 4.5),
            (2, 'Course 2', 'Description 2', 'Tag2, Tag3', 3.8)
        ]
        search = None
        tags = 'Tag1'
        page = 1
        limit = 10
        # Act
        result_courses, result_count = courses_service.get_all_public(page=page, limit=limit, search=search, tags=tags)
        # Assert
        self.assertEqual(result_courses, expected_courses)
        self.assertEqual(result_count, expected_count)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_all_public_without_tags(self, mock_db):
        # Arrange
        expected_courses, expected_count = EXPECTED_PUBLIC_RETURN
        mock_db.read_query.return_value = [
            (1, 'Course 1', 'Description 1', 'Tag1, Tag2', 4.5),
            (2, 'Course 2', 'Description 2', 'Tag2, Tag3', 3.8)
        ]
        search = 'Course 1'
        tags = None
        page = 1
        limit = 10
        # Act
        result_courses, result_count = courses_service.get_all_public(page=page, limit=limit, search=search, tags=tags)
        # Assert
        self.assertEqual(result_courses, expected_courses)
        self.assertEqual(result_count, expected_count)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_all_public_without_query_params(self, mock_db):
        # Arrange
        expected_courses, expected_count = EXPECTED_PUBLIC_RETURN
        mock_db.read_query.return_value = [
            (1, 'Course 1', 'Description 1', 'Tag1, Tag2', 4.5),
            (2, 'Course 2', 'Description 2', 'Tag2, Tag3', 3.8)
        ]
        search = None
        tags = None
        page = 1
        limit = 10
        # Act
        result_courses, result_count = courses_service.get_all_public(page=page, limit=limit, search=search, tags=tags)
        # Assert
        self.assertEqual(result_courses, expected_courses)
        self.assertEqual(result_count, expected_count)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_all(self, mock_db):
        # Arrange
        user = {"user_id": 1, "email": 'email@mail.com', "role": 1}
        expected_courses, expected_count = EXPECTED_LOGGED_IN_RETURN
        mock_db.read_query.return_value = [
            (1, 'Course 1', 'Description 1', 'Objectives 1', 1, 'Tag1, Tag2', 4.5),
            (2, 'Course 2', 'Description 2', 'Objectives 2', 2, 'Tag4', 2.5)
        ]
        search = 'Course 1'
        tags = 'Tag1'
        page = 1
        limit = 10
        # Act
        result_courses, result_count = courses_service.get_all(page=page, limit=limit, search=search, tags=tags)
        # Assert
        self.assertEqual(result_courses, expected_courses)
        self.assertEqual(result_count, expected_count)




    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_all_without_search(self, mock_db):
        # Arrange
        user = {"user_id": 1, "email": 'email@mail.com', "role": 1}
        expected_courses, expected_count = EXPECTED_LOGGED_IN_RETURN
        mock_db.read_query.return_value = [
            (1, 'Course 1', 'Description 1', 'Objectives 1', 1, 'Tag1, Tag2', 4.5),
            (2, 'Course 2', 'Description 2', 'Objectives 2', 2, 'Tag4', 2.5)
        ]
        search = None
        tags = 'Tag4'
        page = 1
        limit = 10
        # Act
        result_courses, result_count = courses_service.get_all(page=page, limit=limit, search=search, tags=tags)
        # Assert
        self.assertEqual(result_courses, expected_courses)
        self.assertEqual(result_count, expected_count)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_all_without_tags(self, mock_db):
        # Arrange
        user = {"user_id": 1, "email": 'email@mail.com', "role": 1}
        expected_courses, expected_count = EXPECTED_LOGGED_IN_RETURN
        mock_db.read_query.return_value = [
            (1, 'Course 1', 'Description 1', 'Objectives 1', 1, 'Tag1, Tag2', 4.5),
            (2, 'Course 2', 'Description 2', 'Objectives 2', 2, 'Tag4', 2.5)
        ]
        search = 'Course 2'
        tags = None
        page = 1
        limit = 10
        # Act
        result_courses, result_count = courses_service.get_all(page=page, limit=limit, search=search, tags=tags)
        # Assert
        self.assertEqual(result_courses, expected_courses)
        self.assertEqual(result_count, expected_count)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_all_without_query_params(self, mock_db):
        # Arrange
        user = {"user_id": 1, "email": 'email@mail.com', "role": 1}
        expected_courses, expected_count = EXPECTED_LOGGED_IN_RETURN
        mock_db.read_query.return_value = [
            (1, 'Course 1', 'Description 1', 'Objectives 1', 1, 'Tag1, Tag2', 4.5),
            (2, 'Course 2', 'Description 2', 'Objectives 2', 2, 'Tag4', 2.5)
        ]
        search = None
        tags = None
        page = 1
        limit = 10
        # Act
        result_courses, result_count = courses_service.get_all(page=page, limit=limit, search=search, tags=tags)
        # Assert
        self.assertEqual(result_courses, expected_courses)
        self.assertEqual(result_count, expected_count)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_subscribed_courses_as_student(self, mock_db):
        # Arrange
        user = {"user_id": 1, "email": 'email@mail.com', "role": 'student'}
        expected = EXPECTED_STUDENT_RETURN
        mock_db.read_query.return_value = [
            (1, 'Course 1', 'Description 1', 'Objectives 1', 1, 'Tag1, Tag2', 4.5),
            (2, 'Course 2', 'Description 2', 'Objectives 2', 2, 'Tag4', 2.5)
        ]
        search = 'Course 1'
        tags = 'Tag1'
        # Act
        result = courses_service.get_subscribed_courses(user=user, search=search, tags=tags)
        # Assert
        self.assertEqual(result, expected)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_subscribed_courses_as_student_without_query_params(self, mock_db):
        # Arrange
        user = {"user_id": 1, "email": 'email@mail.com', "role": 'student'}
        expected = EXPECTED_STUDENT_RETURN
        mock_db.read_query.return_value = [
            (1, 'Course 1', 'Description 1', 'Objectives 1', 1, 'Tag1, Tag2', 4.5),
            (2, 'Course 2', 'Description 2', 'Objectives 2', 2, 'Tag4', 2.5)
        ]
        search = None
        tags = None
        # Act
        result = courses_service.get_subscribed_courses(user=user, search=search, tags=tags)
        # Assert
        self.assertEqual(result, expected)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_subscribed_courses_as_teacher(self, mock_db):
        # Arrange
        user = {"user_id": 2, "email": 'email@mail.com', "role": 'teacher'}
        expected = EXPECTED_TEACHER_RETURN
        mock_db.read_query.return_value = [
            (3, 'Course 3', 'Description 3', 'Objectives 3', 1, 'Tag3, Tag4', 3.8)
        ]
        search = 'Course 3'
        tags = 'Tag3,Tag4'
        # Act
        result = courses_service.get_subscribed_courses(user=user, search=search, tags=tags)
        # Assert
        self.assertEqual(result, expected)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_subscribed_courses_as_teacher_without_query_params(self, mock_db):
        # Arrange
        user = {"user_id": 2, "email": 'email@mail.com', "role": 'teacher'}
        expected = EXPECTED_TEACHER_RETURN
        mock_db.read_query.return_value = [
            (3, 'Course 3', 'Description 3', 'Objectives 3', 1, 'Tag3, Tag4', 3.8)
        ]
        search = None
        tags = None
        # Act
        result = courses_service.get_subscribed_courses(user=user, search=search, tags=tags)
        # Assert
        self.assertEqual(result, expected)



    def test_sort_public_courses_asc_title(self):
        # Arrange
        courses = COURSES
        expected = EXPECTED_ASC_TITLE
        # Act
        result = courses_service.sort_public_courses(courses, attribute='title')
        # Assert
        self.assertEqual(result, expected)



    def test_sort_public_courses_desc_title(self):
        # Arrange
        courses = COURSES
        expected = EXPECTED_DESC_TITLE
        # Act
        result = courses_service.sort_public_courses(courses, attribute='title', reverse=True)
        # Assert
        self.assertEqual(result, expected)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_by_id(self, mock_db):
        # Arrange
        course_id = 1
        expected = CourseResponceModel(
            id=1,
            title='Course 1',
            description='Description 1',
            objectives='Objectives 1',
            type='public',
            tags='Tag1, Tag2',
            rating=4.5
        )
        mock_db.read_query.return_value = [
            (1, 'Course 1', 'Description 1', 'Objectives 1', 1, 'Tag1, Tag2', 4.5)
        ]
        # Act
        result = courses_service.get_by_id(course_id)
        # Assert
        self.assertEqual(result, expected)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_by_id_invalid_id(self, mock_db):
        # Arrange
        course_id = 100  
        mock_db.read_query.return_value = []
        # Act
        result = courses_service.get_by_id(course_id)
        # Assert
        self.assertIsNone(result)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_student_is_subscribed_returns_True(self, mock_db):
        # Arrange
        course_id = 1
        user_id = 2
        mock_db.query_count.return_value = 1
        # Act
        result = courses_service.student_is_subscribed(course_id, user_id)
        # Assert
        self.assertTrue(result)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_student_is_subscribed_returns_False(self, mock_db):
        # Arrange
        course_id = 1
        user_id = 2
        mock_db.query_count.return_value = []
        # Act
        result = courses_service.student_is_subscribed(course_id, user_id)
        # Assert
        self.assertFalse(result)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_subscription_is_pending_returns_True(self, mock_db):
        # Arrange
        course_id = 1
        user_id = 2
        mock_db.query_count.return_value = 1
        # Act
        result = courses_service.subscription_is_pending(course_id, user_id)
        # Assert
        self.assertTrue(result)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_subscription_is_pending_returns_False(self, mock_db):
        # Arrange
        course_id = 1
        user_id = 2
        mock_db.query_count.return_value = 0
        # Act
        result = courses_service.subscription_is_pending(course_id, user_id)
        # Assert
        self.assertFalse(result)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_exists_returns_True(self, mock_db):
        # Arrange
        title = 'Course 1'
        expected_result = True
        mock_db.query_count.return_value = expected_result
        # Act
        result = courses_service.exists(title)
        # Assert
        self.assertTrue(result)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_exists_returns_False(self, mock_db):
        # Arrange
        title = 'Course 1'
        expected_result = False
        mock_db.query_count.return_value = expected_result
        # Act
        result = courses_service.exists(title)
        # Assert
        self.assertFalse(result)



    @mock.patch('services.courses_service.database', autospec=True)
    def test_get_student_limit(self, mock_db):
        # Arrange
        user_id = 1
        expected_result = 0
        mock_db.query_count.return_value = expected_result
        # Act
        result = courses_service.get_student_limit(user_id)
        # Assert
        self.assertEqual(result, expected_result)
