import sys
import os
import unittest
from unittest import mock

# Add the project root directory to the Python module search path
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from data.models_users import StudentInfoForAdmin
from data.models import CourseResponceModelAdmin
from services import admin_service



COURSES_ADMIN_RSPONSE = ([
    CourseResponceModelAdmin(id=1,title='Course 1',description='Description 1', objectives='Objectives 1', type='public', tags='Tag1, Tag2',rating=4.5, students=20),
    CourseResponceModelAdmin(id=2,title='Course 2',description='Description 2', objectives='Objectives 2', type='private', tags='Tag4',rating=2.5, students=10),
], 2)

STUDENTS_ADMIN_RESPONSE = [
    StudentInfoForAdmin(id=1, email="magna@protonmail.couk", deactivated=False, rating=4),
    StudentInfoForAdmin(id=2, email="pretium@aol.edu", deactivated=True, rating=8),
                        ]


class AdminService_Should(unittest.TestCase):
    @mock.patch('services.admin_service.database', autospec=True)
    def test_get_all_courses_returnsListOfCourses(self, mock_db):
        # Arrange
        expected_courses, expected_count = COURSES_ADMIN_RSPONSE
        mock_db.read_query.return_value = [
            (1,'Course 1','Description 1','Objectives 1',1,'Tag1, Tag2',4.5,20),
            (2,'Course 2','Description 2','Objectives 2',2,'Tag4',2.5,10),
        ]
        mock_db.query_count.return_value = 2
        search = None
        page = 1
        limit = 10  
        sort = 'ASC'
        # Act
        result_courses, result_count = admin_service.get_all(search=search, page=page, limit=limit, sort=sort)
        # Assert
        self.assertEqual(result_courses, expected_courses)
        self.assertEqual(result_count, expected_count)

    @mock.patch('services.admin_service.database', autospec=True)
    def test_get_students_who_rated_course_returnsListOfStudents(self, mock_db):
        # Arrange
        expected_students = STUDENTS_ADMIN_RESPONSE
        mock_db.read_query.return_value = [
            (1,"magna@protonmail.couk", False, 4),
            (2,"pretium@aol.edu", True, 8)
            ]
        sort = 'ASC'
        # Act
        result_students = admin_service.get_students_who_rated_course(course_id=1, sort=sort)
        # Assert
        self.assertEqual(result_students, expected_students)

    @mock.patch('services.admin_service.database', autospec=True)
    def test_change_course_status_returnsActivated(self, mock_db):
        # Arrange
        course_id = 1
        status = mock_db.read_query.return_value = [(1,)]
        mock_db.update_query.return_value = 1
        # Act
        result = admin_service.change_course_status(course_id)
        # Assert
        self.assertEqual(result, "activated")

    @mock.patch('services.admin_service.database', autospec=True)
    def test_change_course_status_returnsDeactivated(self, mock_db):
        # Arrange
        course_id = 1
        status = mock_db.read_query.return_value = [(0,)]
        mock_db.update_query.return_value = 1
        # Act
        result = admin_service.change_course_status(course_id)
        # Assert
        self.assertEqual(result, "deactivated")

    @mock.patch('services.admin_service.database', autospec=True)
    def test_change_student_status_returnsActivated(self, mock_db):
        # Arrange
        user_id = 1
        status = mock_db.read_query.return_value = [(1,)]
        mock_db.update_query.return_value = 1
        # Act
        result = admin_service.change_course_status(user_id)
        # Assert
        self.assertEqual(result, "activated")

    @mock.patch('services.admin_service.database', autospec=True)
    def test_change_student_status_returnsDeactivated(self, mock_db):
        # Arrange
        user_id = 1
        status = mock_db.read_query.return_value = [(0,)]
        mock_db.update_query.return_value = 1
        # Act
        result = admin_service.change_course_status(user_id)
        # Assert
        self.assertEqual(result, "deactivated")

    @mock.patch('services.admin_service.database', autospec=True)
    def test_get_students_in_course_returnsStudentCount(self, mock_db):
        # Arrange
        expected_count = 8
        mock_db.query_count.return_value = 8
        # Act
        result_count = admin_service.get_students_in_course(course_id=1)
        # Assert
        self.assertEqual(result_count, expected_count)

    @mock.patch('services.admin_service.database', autospec=True)
    def test_is_subscribed_or_pending_returnsDeleted(self, mock_db):
        # Arrange
        course_id=1
        user_id=1

        mock_db.query_count.return_value = 0
        # Act
        result_count = admin_service.is_subscribed_or_pending(course_id, user_id)
        # Assert
        self.assertFalse(result_count)

    @mock.patch('services.admin_service.database', autospec=True)
    def test_is_subscribed_or_pending_returnsDeleted(self, mock_db):
        # Arrange
        course_id=1
        user_id=1

        mock_db.query_count.return_value = 1
        # Act
        result_count = admin_service.is_subscribed_or_pending(course_id, user_id)
        # Assert
        self.assertTrue(result_count)