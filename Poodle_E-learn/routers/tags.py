from fastapi import APIRouter, Query, Depends
from services import tags_service
from common import auth

tags_router = APIRouter(prefix='/tags', tags=['Tags'])

@tags_router.get('/', responses={
    200: {"description": "All available tags", "content": {"application/json": 
            {"example": {"total_tags": 1, "page": 1, "limit": 10, "tags": [{"id": 1, "name": "tag1"}]}}}}})
def get_all_tags(
    page: int = Query(1, ge=1), 
    limit: int = Query(10, ge=1), 
    user_id: int = Depends(auth.teacher_only)):
    '''Returns all available tags'''

    total_tags = tags_service.count_all_tags()
    offset = (page - 1) * limit

    data = tags_service.get_all_tags(offset, limit)
    tags = [{'id': tag[0], 'name': tag[1]} for tag in data]

    return {
        'total_tags': total_tags,
        'page': page,
        'limit': limit,
        'tags': tags
    }