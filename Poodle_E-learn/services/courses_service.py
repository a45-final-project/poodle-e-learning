from data import database
from data.models import CourseCreate, CourseResponceModelPublic, CourseResponceModel, CourseResponceModelAdmin, CourseTypes, StudentInfoForTeacher



def get_all_public(page: int, limit: int, search: str | None = None, tags: str | None = None) -> tuple[list[CourseResponceModelPublic], int]:
    '''Returns paginated courses and total count where deactivated = 0(false) and type = 1(public)'''

    offset = (page - 1) * limit
    query = '''
        SELECT c.id, c.title, c.description, GROUP_CONCAT(DISTINCT tags.name), AVG(r.rating) AS average_rating
        FROM courses AS c
        LEFT JOIN courses_has_tags AS t ON t.courses_id = c.id
        LEFT JOIN tags ON tags.id = t.tags_id
        LEFT JOIN students_has_ratings AS r ON c.id = r.courses_id
        WHERE deactivated = 0 AND type = 1'''

    count_query = '''SELECT COUNT(*) FROM courses'''
    count_query_condition =' WHERE deactivated = 0 AND type = 1'
    params = []

    if search:
        query += ' AND c.title LIKE ?'
        count_query_condition += ' AND title LIKE ?'
        params.append(f'%{search}%')

    if tags:
        count_query += '''  LEFT JOIN courses_has_tags AS cht ON cht.courses_id = courses.id
                            LEFT JOIN tags ON tags.id = cht.tags_id'''

        query += ' AND tags.name = ?'
        count_query_condition += ' AND tags.name = ?'
        params.append(tags)

    query += ' GROUP BY c.id'
    query += f' LIMIT {limit} OFFSET {offset}'

    count_query += count_query_condition
    count = database.read_query(count_query, params)[0][0]
    data = database.read_query(query, params)
    courses = [CourseResponceModelPublic.from_query_result(*row) for row in data]

    return courses, count



def get_all(page: int, limit: int, search: str | None = None, tags: str | None = None) -> tuple[list[CourseResponceModel], int]:
    '''Returns all courses where deactivated = 0(false) for teachers and students'''

    offset = (page - 1) * limit
    query = '''
        SELECT c.id, c.title, c.description, c.objectives, c.type, GROUP_CONCAT(DISTINCT t.name) AS tags, AVG(r.rating) AS rating
        FROM courses AS c
        LEFT JOIN courses_has_tags AS ct ON c.id = ct.courses_id
        LEFT JOIN tags AS t ON ct.tags_id = t.id
        LEFT JOIN students_has_courses AS sc ON c.id = sc.courses_id
        LEFT JOIN students_has_ratings AS r ON c.id = r.courses_id
        WHERE c.deactivated = 0'''

    count_query = 'SELECT COUNT(*) FROM courses'
    count_query_condition =' WHERE deactivated = 0'
    params = []

    if search:
        query += ' AND c.title LIKE ?'
        count_query_condition += ' AND title LIKE ?'
        params.append(f'%{search}%')

    if tags:
        count_query += '''
                        LEFT JOIN courses_has_tags AS cht ON cht.courses_id = courses.id
                        LEFT JOIN tags AS t ON t.id = cht.tags_id
                        '''

        query += ' AND t.name = ?'
        count_query_condition += ' AND t.name = ?'
        params.append(tags)

    query += ' GROUP BY c.id'
    query += f' LIMIT {limit} OFFSET {offset}'

    count_query += count_query_condition
    count = database.read_query(count_query, params)[0][0]
    data = database.read_query(query, params)
    courses = [CourseResponceModel.from_query_result(*row) for row in data]

    return courses, count



def get_subscribed_courses(user: dict, search: str | None = None, tags: str | None = None) -> CourseResponceModelAdmin | CourseResponceModel | None:
    '''Returns all courses a student is subscribed to OR all courses a teacher has'''

    if user['role'] == 'student':
        query = f'''
            SELECT c.id, c.title, c.description, c.objectives, c.type, GROUP_CONCAT(DISTINCT t.name) AS tags, AVG(r.rating) AS rating
            FROM courses AS c
            LEFT JOIN courses_has_tags AS ct ON c.id = ct.courses_id
            LEFT JOIN tags AS t ON ct.tags_id = t.id
            LEFT JOIN students_has_courses AS sc ON c.id = sc.courses_id
            LEFT JOIN students_has_ratings AS r ON c.id = r.courses_id
            WHERE c.deactivated = 0
            AND sc.users_id = {user["user_id"]}'''
        
        params = []
        if search:
            query += ' AND c.title LIKE ?'
            params.append(f'%{search}%')
        if tags:
            query += ' AND t.name = ?'
            params.append(tags)

        query += ' GROUP BY c.id'
        data = database.read_query(query, params)
        return [CourseResponceModel.from_query_result(*row) for row in data]

    else:
        query = f'''
            SELECT c.id, c.title, c.description, c.objectives, c.type, GROUP_CONCAT(DISTINCT t.name) AS tags, AVG(r.rating) AS rating
            FROM courses AS c
            LEFT JOIN courses_has_tags AS ct ON c.id = ct.courses_id
            LEFT JOIN tags AS t ON ct.tags_id = t.id
            LEFT JOIN teachers_has_courses AS tc ON c.id = tc.courses_id
            LEFT JOIN students_has_ratings AS r ON c.id = r.courses_id
            WHERE c.deactivated = 0
            AND tc.users_id = {user["user_id"]}'''
        
        params = []
        if search:
            query += ' AND c.title LIKE ?'
            params.append(f'%{search}%')
        if tags:
            query += ' AND t.name = ?'
            params.append(tags)

        query += ' GROUP BY c.id'
        data = database.read_query(query, params)
        return [CourseResponceModel.from_query_result(*row) for row in data]



def sort_public_courses(courses: list[CourseResponceModelPublic], *, attribute='tags', reverse=False):
    '''Sorting function that sorts def get_courses_as_anonymous()'''
    if attribute == 'tags':
        def sort_fn(c: CourseResponceModelPublic): return c.tags
    elif attribute == 'title':
        def sort_fn(c: CourseResponceModelPublic): return c.title
    else:
        def sort_fn(c: CourseResponceModelPublic): return c.id
    return sorted(courses, key=sort_fn, reverse=reverse)



def sort_courses(courses: list[CourseResponceModel], *, attribute='tags', reverse=False):
    '''Sorting function that sorts def get_all_courses()'''
    if attribute == 'tags':
        def sort_fn(c: CourseResponceModel): return c.tags
    elif attribute == 'title':
        def sort_fn(c: CourseResponceModel): return c.title
    elif attribute == 'rating':
        def sort_fn(c: CourseResponceModel): return c.rating   
    else:
        def sort_fn(c: CourseResponceModel): return c.id
    return sorted(courses, key=sort_fn, reverse=reverse)



def get_by_id(course_id: int) -> CourseResponceModel | None:
    '''Returns course by course id'''

    query = '''
        SELECT c.id, c.title, c.description, c.objectives, c.type,
               GROUP_CONCAT(DISTINCT t.name) AS tags, AVG(r.rating)
        FROM courses AS c
        LEFT JOIN courses_has_tags AS cht ON c.id = cht.courses_id
        LEFT JOIN tags AS t ON cht.tags_id = t.id
        LEFT JOIN students_has_ratings AS r ON c.id = r.courses_id
        WHERE c.id = ?
    '''
    query +=' GROUP BY c.id'
    params = (course_id,)
    data = database.read_query(query, params)
    return next((CourseResponceModel.from_query_result(*row) for row in data), None)


def unsubscribe_student(course_id: int, user_id: int) -> None:
    '''Unsubscribes student from a course'''

    database.update_query('''
        UPDATE students_has_courses SET subscription = 0
        WHERE courses_id = ? AND users_id = ?''', (course_id, user_id))


def student_is_subscribed(course_id: int, user_id: int) -> bool:
    '''Returns True if student is subscribed to the course or False if not'''

    data = database.query_count(f'''
            SELECT COUNT(*) AS is_subscribed
            FROM students_has_courses
            WHERE users_id = {user_id} AND courses_id = {course_id} AND subscription = 1''')

    return data


def subscription_is_pending(course_id: int, user_id: int) -> bool:
    '''Returns True if student waits for approwal from teacher'''

    data = database.query_count('''
            SELECT COUNT(*) AS is_subscribed
            FROM students_has_courses
            WHERE users_id = ? AND courses_id = ? AND subscription = 3''', (user_id, course_id))

    return data


def exists(title: str) -> bool:
    '''Returns True if the course title exists or False if not'''

    data = database.query_count('''SELECT EXISTS(SELECT 1 FROM courses WHERE title = ?) AS title_exists''',(title,))

    return data


def create(course: CourseCreate, teacher_id: int) -> CourseCreate:
    '''Creates a new course as a teacher'''

    course_type_int = CourseTypes.STR_TO_INT[course.type]
    generated_id = database.insert_query(
        '''INSERT INTO courses (title, description, objectives, type) VALUES(?,?,?,?)''',
        (course.title, course.description, course.objectives, course_type_int))
    database.insert_query(
        '''INSERT INTO teachers_has_courses (users_id, courses_id) VALUES(?,?)''',
        (teacher_id, generated_id))
    data = database.read_query(''' 
            SELECT c.id, c.title, c.description, c.objectives, c.type, GROUP_CONCAT(DISTINCT t.name) AS tags, AVG(r.rating) AS rating
            FROM courses AS c
            LEFT JOIN courses_has_tags AS ct ON c.id = ct.courses_id
            LEFT JOIN tags AS t ON ct.tags_id = t.id
            LEFT JOIN students_has_ratings AS r ON c.id = r.courses_id
            WHERE c.id = ?
            GROUP BY c.id, c.title, c.description, c.objectives, c.type;''', (generated_id,))

    return (CourseResponceModel.from_query_result(*row) for row in data)


def get_student_limit(user_id: int) -> int:
    '''Returns the number(int) of private subscriptions a student has'''

    data = database.query_count('''
            SELECT COUNT(*) AS private_subscription_count
            FROM students_has_courses AS shc
            JOIN courses AS c ON shc.courses_id = c.id
            WHERE shc.users_id = ?
            AND c.type = 2''', (user_id,))

    return data


def subscribe_student(course_id: int, user_id: int) -> None:
    '''Subscribes a student to a course'''

    subscription_pending = 3
    database.insert_query(
        '''INSERT INTO students_has_courses (users_id, courses_id, subscription) VALUES(?,?,?)''',
    (user_id, course_id, subscription_pending))


def is_subscribed(user_id:int, course_id:int) -> bool:
    '''Returns True if the student is subscribed to the course'''

    result = database.read_query(
        '''SELECT * FROM students_has_courses WHERE users_id = ? and courses_id = ?''', 
        (user_id, course_id))
    return any(result)


def is_owner(course_id: int, user_id: int) -> bool:
    '''Returns True if the teacher is the owner of the course and False if not'''

    result = database.read_query(
        f'''SELECT * FROM teachers_has_courses WHERE users_id = ? AND courses_id = ?''', 
        (user_id, course_id))
    return any(result)


def get_enrollment_request(course_id: int, student_id: int) -> bool:
    '''Returns True if the student subscription_status for the course is 3 and False if not'''

    query = '''
            SELECT *
            FROM students_has_courses
            WHERE courses_id = ? AND users_id = ? AND subscription = 3
            '''
    params = (course_id, student_id)
    result = database.read_query(query, params)

    return any(result)


def approve_enrollment_request(course_id: int, student_id: int) -> None:
    '''Update the subscription status to 1 (approved) for the specified course and student'''

    query = '''
            UPDATE students_has_courses
            SET subscription = 1
            WHERE courses_id = ? AND users_id = ? AND subscription = 3
            '''
    params = (course_id, student_id)
    database.update_query(query, params)


def approve_all_requests(course_id: int) -> list:
    '''Update the subscription status to 1 (approved) for all pending requests of the specified course'''
    
    # Updates student's subscription with status = 3(pending) to status1(approved)
    database.update_query('''
            UPDATE students_has_courses
            SET subscription = 1
            WHERE courses_id = ? AND subscription = 3''', (course_id,))
    
    # Retrieves the IDs of the students whose subscription status was changed 3(pending) to status1(approved)
    data = database.read_query('''
            SELECT users_id
            FROM students_has_courses
            WHERE courses_id = ? AND subscription = 1''',(course_id,))
    return [row[0] for row in data]


def rate(course_id: int, rating: int, user_id: int) -> None:
    '''Student rates the course he is subscribed to'''

    data = database.query_count('SELECT COUNT(*) FROM students_has_ratings WHERE users_id = ? AND courses_id = ? ', (user_id,course_id))
    
    if data == 0:
        database.insert_query('''INSERT INTO students_has_ratings(users_id, courses_id, rating)
        VALUES (?,?,?)''', (user_id, course_id, rating))
    else:
        database.update_query('''UPDATE students_has_ratings
        SET rating = ?
        WHERE courses_id=? AND users_id=?''', (rating, course_id, user_id))

    

def aggregate_ratings(course_id: int):
    '''Returns the rating of a course'''

    data = database.read_query('''SELECT courses_id, AVG(rating)
            FROM students_has_ratings
            WHERE courses_id = ?
            GROUP BY courses_id;''', (course_id,))
    return f"{data[0][1]:.1f}"


def all_subscriptions(course_id: int):
    data = database.query_count(
        '''SELECT COUNT(*) FROM students_has_courses 
        WHERE courses_id = ? AND subscription !=3''', (course_id,))

    return data

# Returns the id of the tag
def get_tag_by_name(tag_name: str) -> int | None:
    query = '''
            SELECT id
            FROM tags
            WHERE name LIKE %s
            '''    
    params = (tag_name,)
    data = database.read_query(query, params)
    if len(data) > 0:
        return data[0][0]
    return None


# Creates a new tag and returns its id
def create_tag(tag_name: str) -> int:
    query = '''
            INSERT INTO tags(name) 
            VALUES (?)
            '''   
    params = (tag_name,)
    data = database.insert_query(query, params)    
    return data


# Assigngns one or multiple tags to already existing course and returns their id/s
def assign_tags_to_course(course_id: int, created_tags: list[str]) -> int:
    database.update_query('''DELETE FROM courses_has_tags WHERE courses_id =?''', (course_id,))
    query = '''
            INSERT INTO courses_has_tags (courses_id, tags_id)
            VALUES 
    '''
    temp = []
    for tag in created_tags:
        temp.append(f'({course_id}, {tag})')
    query+=','.join(temp)
    data = database.insert_query(query)
    return data


# Returns the number of all available tags
def count_all_tags() -> int:
    query = '''
            SELECT COUNT(*) AS total_tags
            FROM tags
            '''
    result = database.read_query(query)
    total_tags = result[0][0] if result else 0
    return total_tags


# Returns a list of all available tags
def get_all_tags_pagination(offset: int, page_size: int) -> list:
    query = '''
            SELECT id, name
            FROM tags
            LIMIT %s, %s
            '''
    params = (offset, page_size)
    data = database.read_query(query, params)
    return data


def get_students_who_subscribed_to_course(course_id:int, sort:str|None = None):
    '''Returns past and present students who subscribed to the course'''

    query = '''SELECT u.id, u.mail, u.deactivated, r.subscription 
            FROM students_has_courses AS r 
            LEFT JOIN users AS u ON r.users_id = u.id
            WHERE courses_id = ?'''

    if sort:
        query += f' ORDER BY r.subscription {sort}'

    params = (course_id,)
    data = database.read_query(query, params)
    return [StudentInfoForTeacher.query_result(*row) for row in data]