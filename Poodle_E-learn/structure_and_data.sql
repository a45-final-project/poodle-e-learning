CREATE DATABASE  IF NOT EXISTS `mariadbtest` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mariadbtest`;
-- MySQL dump 10.13  Distrib 8.0.33, for Win64 (x86_64)
--
-- Host: mariadbtest.cblr3ahtvqpq.eu-north-1.rds.amazonaws.com    Database: mariadbtest
-- ------------------------------------------------------
-- Server version	5.5.5-10.6.10-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL,
  `description` varchar(500) NOT NULL,
  `objectives` varchar(500) NOT NULL,
  `type` int(11) NOT NULL,
  `deactivated` tinyint(4) NOT NULL DEFAULT 0,
  `home_page` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (1,'Introduction to Python Programming','Learn the fundamentals of Python programming, including data types, control structures, and functions.','By the end of this course, students will be able to write Python programs to solve basic computational problems, understand the principles of object-oriented programming, and apply their knowledge to develop simple applications.',1,0,'asdf.com'),(2,'Web Development Fundamentals','Explore the core concepts of web development, including HTML, CSS, and JavaScript, and learn how to create static web pages.','This course aims to provide students with the skills to design and build visually appealing web pages using HTML, apply CSS for layout and styling, and implement interactivity using JavaScript.',1,0,NULL),(3,'Data Structures and Algorithms','Dive into essential data structures such as arrays, linked lists, stacks, queues, and trees, and learn algorithms for searching, sorting, and graph traversal.','The objectives of this course are to develop a solid understanding of various data structures, analyze algorithm complexity, and apply efficient algorithms to solve real-world problems.',2,0,'qwert.com'),(4,'Object-Oriented Programming in Java','Gain a comprehensive understanding of object-oriented programming concepts using the Java programming language.','By the end of this course, students will be able to design and implement object-oriented solutions, work with classes, objects, inheritance, and polymorphism in Java, and develop robust applications.',2,0,NULL),(5,'Introduction to Database Management Systems','Learn the basics of database design, normalization, and SQL, and explore the principles of relational database management systems.','This course aims to equip students with the knowledge to design and build relational databases, write SQL queries to extract and manipulate data, and understand the principles of database management systems.',1,1,NULL),(6,'Mobile App Development with React Native','Discover the principles of cross-platform mobile app development using React Native, including UI design, state management, and integrating APIs.','By the end of this course, students will be able to develop mobile applications using React Native, apply UI design principles, manage application state, and integrate external APIs for data retrieval.',2,0,NULL),(7,'Introduction to Data Science with Python','Explore the field of data science by learning how to analyze and manipulate data using Python libraries like NumPy, Pandas, and Matplotlib.','This course aims to provide students with the foundational knowledge to extract insights from data, perform data wrangling and visualization, and apply statistical analysis techniques.',2,0,NULL),(8,'Web Application Development with Django','Learn how to build dynamic web applications using the Python-based Django framework, covering topics such as models, views, templates, and authentication.','By the end of this course, students will be able to create database-driven web applications using Django, implement user authentication and authorization, and deploy their applications to a web server.',2,0,NULL),(9,'Agile Software Development','Understand the principles and practices of agile software development, including Scrum, Kanban, and continuous integration.','This course aims to familiarize students with agile development methodologies, teach them how to work effectively in agile teams, and enable them to deliver high-quality software iteratively.',2,0,NULL),(10,'Introduction to Machine Learning','Explore the concepts and techniques of machine learning, including supervised and unsupervised learning algorithms, model evaluation, and feature engineering.','The objectives of this course are to provide students with a foundational understanding of machine learning algorithms, enable them to apply these algorithms to solve real-world problems, and introduce them to the ethical considerations in machine learning.',1,0,NULL),(11,'Advanced JavaScript and Front-End Frameworks','Dive deeper into JavaScript by learning advanced concepts and exploring popular front-end frameworks like React or Vue.js.','By the end of this course, students will have an in-depth understanding of JavaScript, be proficient in using front-end frameworks, and be able to develop interactive and dynamic web applications.',1,0,NULL),(12,'Secure Coding Practices','Learn best practices for writing secure code, including input validation, data sanitization, and protecting against common vulnerabilities.','This course aims to equip students with the knowledge and skills to write secure code, identify and mitigate common security vulnerabilities, and follow industry standards for secure software development.',1,1,NULL),(13,'Full-Stack Web Development','Master both front-end and back-end web development, covering technologies such as HTML/CSS, JavaScript, Node.js, and databases.','By the end of this course, students will be able to design and develop full-stack web applications, implement RESTful APIs, and utilize databases for persistent data storage.',1,0,NULL),(14,'Introduction to Data Visualization','Learn how to effectively visualize data using popular libraries and tools like Matplotlib, D3.js, and Tableau.','This course aims to develop students\' skills in creating visually appealing and informative data visualizations, enabling them to communicate insights effectively and make data-driven decisions.',1,0,NULL),(15,'Software Testing and Quality Assurance','Gain practical knowledge of software testing methodologies, techniques, and tools, and learn how to ensure the quality of software applications.','The objectives of this course are to teach students various software testing techniques, equip them with skills in creating test cases and executing test plans, and familiarize them with tools for automated testing.',2,0,NULL),(16,'Cloud Computing Fundamentals','Explore the fundamental concepts of cloud computing, including virtualization, containers, and cloud service models (IaaS, PaaS, SaaS).','This course aims to provide students with an understanding of cloud computing technologies, enable them to deploy applications in cloud environments, and introduce them to cloud security and scalability.',2,0,NULL),(17,'iOS App Development with Swift','Learn how to develop native iOS applications using the Swift programming language and the iOS development ecosystem.','By the end of this course, students will be able to design and build iOS apps, utilize the UIKit framework, work with different UI elements, and implement data persistence.',1,1,NULL),(18,'Backend Development with Node.js','Dive into backend web development using Node.js, covering topics such as routing, middleware, database integration, and API development.','This course aims to equip students with the skills to build scalable and efficient server-side applications using Node.js, interact with databases, and develop RESTful APIs.',1,1,NULL),(19,'Introduction to Artificial Intelligence','Explore the fundamentals of artificial intelligence, including machine learning, natural language processing, and computer vision.','The objectives of this course are to provide students with an overview of artificial intelligence concepts and techniques, enable them to understand the potential applications of AI, and introduce ethical considerations in AI development.',1,0,NULL),(20,'DevOps and Continuous Integration/Continuous Deployment (CI/CD)','Learn how to streamline the software development process by implementing DevOps practices and CI/CD pipelines.','This course aims to familiarize students with DevOps principles, introduce them to CI/CD methodologies and tools, and enable them to automate the software delivery process.',2,0,NULL),(21,'Introduction to Cybersecurity','Gain a basic understanding of cybersecurity principles, including threat analysis, cryptography, network security, and secure coding practices.','The objectives of this course are to introduce students to the field of cybersecurity, provide an overview of common security threats, and teach them fundamental strategies to protect against cyber attacks.',2,0,NULL),(22,'Game Development with Unity','Learn the basics of game development using the Unity game engine, including game mechanics, physics, and asset integration.','By the end of this course, students will be able to design and develop simple games using Unity, implement player controls and interactions, and understand the game development pipeline.',1,0,NULL),(23,'Big Data Analytics','Explore the concepts and techniques for analyzing large datasets, including data preprocessing, distributed computing frameworks, and machine learning on big data.','This course aims to provide students with the knowledge and skills to handle and analyze big data, utilize distributed computing frameworks like Hadoop and Spark, and apply machine learning algorithms to large datasets.',1,1,NULL),(24,'Android App Development with Kotlin','Learn how to develop native Android applications using the Kotlin programming language and the Android development ecosystem.','By the end of this course, students will be able to design and build Android apps, implement UI components, handle user interactions, and utilize device features like sensors and location services.',1,0,NULL),(25,'Introduction to Robotics Programming','Dive into the world of robotics programming, learning how to control robots using programming languages and frameworks such as ROS (Robot Operating System).','This course aims to provide students with the foundational knowledge of robotics programming, enabling them to control robots, perform basic navigation, and integrate sensors and actuators.',1,0,NULL),(26,'Natural Language Processing (NLP)','Explore the field of natural language processing, covering topics such as text preprocessing, sentiment analysis, named entity recognition, and language modeling.','The objectives of this course are to introduce students to the techniques used in natural language processing, enable them to apply NLP algorithms to analyze textual data, and understand the challenges in NLP.',2,1,NULL),(27,'Software Engineering Principles','Learn the principles and best practices of software engineering, including requirements gathering, software design, testing, and project management.','This course aims to equip students with a solid foundation in software engineering, teach them the process of software development, and familiarize them with industry-standard tools and methodologies.',1,0,NULL),(28,'Blockchain Fundamentals','Gain an understanding of blockchain technology, including decentralized ledgers, smart contracts, and cryptocurrencies.','The objectives of this course are to introduce students to the fundamentals of blockchain, provide an overview of various blockchain platforms, and explore the potential applications of blockchain beyond cryptocurrencies.',2,0,NULL),(29,'Scalable Web Architecture and Performance Optimization','Learn how to design and build scalable web architectures, optimize performance, and handle high traffic loads.','This course aims to equip students with the knowledge to design and implement scalable web architectures, apply performance optimization techniques, and utilize caching and load balancing strategies.',2,1,NULL),(30,'Computer Graphics and Visualization','Explore the principles and techniques of computer graphics, covering topics such as 2D/3D rendering, shaders, and interactive visualization.','By the end of this course, students will have a solid understanding of computer graphics concepts, be able to implement basic rendering algorithms, and create interactive visualizations using appropriate libraries and tools.',2,1,NULL),(31,'Game Development with Unreal Engine','Dive into the world of game development using Unreal Engine, one of the industry\'s leading game engines, and learn the essentials of creating interactive and visually stunning games.','This course aims to provide students with the knowledge and skills to design and develop games using Unreal Engine, including level design, character animation, physics simulation, and implementing game mechanics. By the end of the course, students will be able to create their own playable game projects using Unreal Engine\'s powerful features and tools.',1,0,NULL);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_has_tags`
--

DROP TABLE IF EXISTS `courses_has_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courses_has_tags` (
  `courses_id` int(11) NOT NULL,
  `tags_id` int(11) NOT NULL,
  PRIMARY KEY (`courses_id`,`tags_id`),
  KEY `fk_courses_has_tags_tags1_idx` (`tags_id`),
  KEY `fk_courses_has_tags_courses1_idx` (`courses_id`),
  CONSTRAINT `fk_courses_has_tags_courses1` FOREIGN KEY (`courses_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_courses_has_tags_tags1` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_has_tags`
--

LOCK TABLES `courses_has_tags` WRITE;
/*!40000 ALTER TABLE `courses_has_tags` DISABLE KEYS */;
INSERT INTO `courses_has_tags` VALUES (1,25),(1,26),(2,35),(2,36),(3,18),(3,26),(4,31),(4,37),(5,23),(5,24),(6,28),(6,30),(7,11),(7,12),(7,25),(8,25),(8,32),(9,38),(10,21),(11,1),(11,2),(11,3),(13,2),(13,9),(13,20),(13,38),(14,39),(15,34),(15,40),(16,15),(17,29),(17,30),(18,1),(18,9),(18,10),(19,21),(20,16),(20,17),(21,22),(22,19),(22,41),(23,11),(23,12),(24,7),(24,8),(24,30),(25,27),(26,21),(26,33),(27,5),(28,13),(28,14),(29,42),(30,43),(30,44),(31,19),(31,45);
/*!40000 ALTER TABLE `courses_has_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courses_id` int(11) NOT NULL,
  `title` varchar(80) NOT NULL,
  `content_type` varchar(45) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `external_info` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sections_courses1_idx` (`courses_id`),
  CONSTRAINT `fk_sections_courses1` FOREIGN KEY (`courses_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (1,1,'Python Programming Fundamentals','Video','Learn the fundamentals of Python programming language and its applications.','asdg.com'),(2,1,'Hands-on Coding Exercises','PDF','Practice coding exercises and solve Python programming challenges.','asdg.com'),(3,1,'Quiz and Assessments','Quiz','Test your understanding of Python concepts with interactive quizzes.',NULL),(4,2,'Introduction to Web Development','PowerPoint','Explore the basics of web development, including HTML, CSS, and JavaScript.',NULL),(5,2,'Building Web Pages with HTML, CSS, and JavaScript','Tutorial	','Follow step-by-step tutorials to build simple web pages and interactive features.','asdg.com'),(6,2,'Case Studies in Web Development','Case Study','Analyze real-world web development projects and discuss their implementation.','asdg.com'),(7,3,'Fundamental Data Structures','PDF','	Gain knowledge about fundamental data structures and algorithms used in computer science.',NULL),(8,3,'Algorithms and Problem Solving','Code Repository','Access a collection of code snippets and implementations of various data structures and algorithms.',NULL),(9,3,'Code Implementation and Problem Set','Problem Set','Solve coding problems that involve data structures and algorithmic techniques.',NULL),(10,4,'Introduction to Object-Oriented Programming','Video','	Dive into object-oriented programming concepts using the Java programming language.','asdg.com'),(11,4,'Applying OOP Principles in Java','Assignment','Complete programming assignments that focus on applying OOP principles in Java.',NULL),(12,4,'Project Assignments and Discussion','Discussion Forum','Engage in online discussions about OOP concepts, design patterns, and best practices in Java.',NULL),(13,5,'Database Management Fundamentals','Video','Understand the fundamentals of database management systems and SQL queries.',NULL),(14,5,'SQL Queries and Operations','Lab Exercises','Practice writing SQL queries and perform database operations in a hands-on lab environment.',NULL),(15,5,'Case Studies in Database Design','Case Study','Analyze real-world database scenarios and design efficient database schemas.',NULL),(16,6,'Introduction to React Native','Video','Learn how to develop cross-platform mobile applications using React Native framework.',NULL),(17,6,'Building UI and Components','Code Walkthrough','Follow along with code walkthroughs to understand the implementation of mobile app features.',NULL),(18,6,'Project Showcase and Deployment','Project Showcase','Showcase your own React Native mobile app project and share insights with others.',NULL),(19,7,'Foundations of Data Science','Video','Discover the essentials of data science using Python and its popular libraries.',NULL),(20,7,'Data Manipulation and Analysis with Python','Jupyter Notebook','Access interactive Jupyter notebooks with code examples and data science workflows.',NULL),(21,7,'Practical Data Science Tasks','Data Analysis Task','Perform a guided data analysis task using Python and data manipulation techniques.',NULL),(22,8,'Introduction to Django Framework','Video','Build dynamic web applications using the Django web framework in Python.',NULL),(23,8,'Building Web Applications with Django','Documentation','Access comprehensive documentation on Django\'s features and APIs.',NULL),(24,8,'Deployment and Advanced Features','Deployment Guide','Learn how to deploy Django applications to production servers and cloud platforms.',NULL),(25,9,'Agile Methodologies and Principles','PDF','Explore the principles and practices of Agile software development methodologies.',NULL),(26,9,'Agile Project Management and Practices','Case Studies','Analyze real-world case studies of Agile development projects and discuss lessons learned.',NULL),(27,9,'Team Collaboration and Simulation','Team Simulation	','Participate in a team simulation activity to experience Agile principles in action.',NULL),(28,10,'Introduction to Machine Learning Concepts','Video','Get an introduction to machine learning algorithms and their applications.',NULL),(29,10,'Implementing Machine Learning Algorithms','Coding Exercises','Practice implementing machine learning algorithms and models in Python.',NULL),(30,10,'Model Evaluation and Interpretation','Model Evaluation','Learn how to evaluate and interpret the performance of machine learning models.',NULL);
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students` (
  `users_id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  PRIMARY KEY (`users_id`),
  KEY `fk_students_users1_idx` (`users_id`),
  CONSTRAINT `fk_students_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (4,'Grace','Woodward'),(5,'Ann','Bowman'),(6,'Livvie','Alenichev'),(7,'Alphonse','Sinclaire'),(11,'Lazar','Roden'),(12,'Simonne','Carnilian'),(13,'Malissa','Krug'),(16,'Bobby','Bobbson');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_has_courses`
--

DROP TABLE IF EXISTS `students_has_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students_has_courses` (
  `users_id` int(11) NOT NULL,
  `courses_id` int(11) NOT NULL,
  `subscription` int(11) NOT NULL,
  PRIMARY KEY (`users_id`,`courses_id`),
  KEY `fk_students_has_courses_courses1_idx` (`courses_id`),
  KEY `fk_students_has_courses_students1_idx` (`users_id`),
  CONSTRAINT `fk_students_has_courses_courses1` FOREIGN KEY (`courses_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_students_has_courses_students1` FOREIGN KEY (`users_id`) REFERENCES `students` (`users_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_has_courses`
--

LOCK TABLES `students_has_courses` WRITE;
/*!40000 ALTER TABLE `students_has_courses` DISABLE KEYS */;
INSERT INTO `students_has_courses` VALUES (4,1,1),(4,2,1),(4,5,2),(5,1,1),(5,4,1),(5,6,3),(6,3,1),(6,14,1),(6,30,2),(7,9,1),(7,18,2),(7,27,3),(11,5,2),(11,9,1),(11,16,1),(11,23,1),(12,20,3),(12,26,2),(13,31,1),(16,7,1),(16,15,1),(16,19,2),(16,31,1);
/*!40000 ALTER TABLE `students_has_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_has_ratings`
--

DROP TABLE IF EXISTS `students_has_ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students_has_ratings` (
  `users_id` int(11) NOT NULL,
  `courses_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`users_id`,`courses_id`),
  KEY `fk_students_has_courses1_courses1_idx` (`courses_id`),
  KEY `fk_students_has_courses1_students1_idx` (`users_id`),
  CONSTRAINT `fk_students_has_courses1_courses1` FOREIGN KEY (`courses_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_students_has_courses1_students1` FOREIGN KEY (`users_id`) REFERENCES `students` (`users_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_has_ratings`
--

LOCK TABLES `students_has_ratings` WRITE;
/*!40000 ALTER TABLE `students_has_ratings` DISABLE KEYS */;
INSERT INTO `students_has_ratings` VALUES (4,1,10),(4,2,6),(4,5,2),(5,1,8),(5,3,7),(6,3,1),(6,14,6),(6,30,9),(7,9,10),(11,5,1),(11,9,9),(11,23,4),(13,31,8),(16,7,6),(16,15,9),(16,31,10);
/*!40000 ALTER TABLE `students_has_ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_has_sections`
--

DROP TABLE IF EXISTS `students_has_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students_has_sections` (
  `users_id` int(11) NOT NULL,
  `sections_id` int(11) NOT NULL,
  `visited` int(11) DEFAULT 1,
  PRIMARY KEY (`users_id`,`sections_id`),
  KEY `fk_students_has_sections_sections1_idx` (`sections_id`),
  KEY `fk_students_has_sections_students1_idx` (`users_id`),
  CONSTRAINT `fk_students_has_sections_sections1` FOREIGN KEY (`sections_id`) REFERENCES `sections` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_students_has_sections_students1` FOREIGN KEY (`users_id`) REFERENCES `students` (`users_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_has_sections`
--

LOCK TABLES `students_has_sections` WRITE;
/*!40000 ALTER TABLE `students_has_sections` DISABLE KEYS */;
INSERT INTO `students_has_sections` VALUES (4,1,1),(4,2,1),(5,8,1),(5,10,1),(6,8,1),(7,25,1),(7,26,1),(7,27,1),(11,25,1),(11,26,1),(16,20,1),(16,21,1);
/*!40000 ALTER TABLE `students_has_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'JavaScript'),(2,'Front-End Development'),(3,'Frameworks'),(4,'Agile'),(5,'Software Development'),(6,'Project Management'),(7,'Android Development'),(8,'Kotlin'),(9,'Backend Development'),(10,'Node.js'),(11,'Big Data'),(12,'Data Science'),(13,'Blockchain Technology'),(14,'Cryptocurrency'),(15,'Cloud Infrastructure'),(16,'DevOps'),(17,'CI/CD'),(18,'Data Structures'),(19,'Game Development'),(20,'Full-Stack Development'),(21,'Machine Learning'),(22,'Cybersecurity'),(23,'Database Management'),(24,'SQL'),(25,'Python'),(26,'Programming Basics'),(27,'Robotics'),(28,'React'),(29,'Swift'),(30,'Mobile App Development'),(31,'Java'),(32,'Django'),(33,'NLP'),(34,'Test Automation'),(35,'Web Design'),(36,'HTML/CSS'),(37,'Object-Oriented Programming'),(38,'Web Development'),(39,'Visual Analytics'),(40,'Quality Assurance'),(41,'Unity'),(42,'Web Scalability'),(43,'3D Modeling'),(44,'Computer Graphics'),(45,'Unreal Engine');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teachers` (
  `users_id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `linked_in` varchar(200) NOT NULL,
  PRIMARY KEY (`users_id`),
  KEY `fk_teachers_users1_idx` (`users_id`),
  CONSTRAINT `fk_teachers_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers`
--

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` VALUES (2,'Jordan','Johnson','+4388888888','https://www.linkedin.com/feed/'),(3,'Leonard','Cruz','87654321','https://www.linkedin.com/feed/'),(8,'Jobey','Van de Vlies','+393709833516','https://www.linkedin.com/in/jobey-vdv'),(9,'Tamara','Higgonet','+861406181004','https://www.linkedin.com/in/tamara-h'),(10,'Lind','Cato','+621808271044','https://www.linkedin.com/in/lind-c'),(14,'Ethelind','Pawelczyk','+861661438346','https://www.linkedin.com/in/ethelind-p'),(15,'James','Jameson','+2378016677','https://www.linkedin.com/in');
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers_has_courses`
--

DROP TABLE IF EXISTS `teachers_has_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teachers_has_courses` (
  `users_id` int(11) NOT NULL,
  `courses_id` int(11) NOT NULL,
  PRIMARY KEY (`users_id`,`courses_id`),
  KEY `fk_teachers_has_courses_courses1_idx` (`courses_id`),
  KEY `fk_teachers_has_courses_teachers1_idx` (`users_id`),
  CONSTRAINT `fk_teachers_has_courses_courses1` FOREIGN KEY (`courses_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_teachers_has_courses_teachers1` FOREIGN KEY (`users_id`) REFERENCES `teachers` (`users_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers_has_courses`
--

LOCK TABLES `teachers_has_courses` WRITE;
/*!40000 ALTER TABLE `teachers_has_courses` DISABLE KEYS */;
INSERT INTO `teachers_has_courses` VALUES (2,1),(2,2),(2,5),(2,6),(2,9),(2,10),(3,3),(3,4),(3,7),(3,8),(3,13),(8,7),(8,12),(8,16),(8,17),(8,22),(8,27),(9,3),(9,9),(9,18),(9,21),(9,29),(10,4),(10,6),(10,14),(10,20),(10,24),(10,25),(14,5),(14,11),(14,19),(14,26),(14,31),(15,2),(15,8),(15,15),(15,23),(15,28),(15,30);
/*!40000 ALTER TABLE `teachers_has_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(45) NOT NULL,
  `password` varchar(200) NOT NULL,
  `role` int(11) NOT NULL,
  `deactivated` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mail_UNIQUE` (`mail`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'dictum.placerat.augue@icloud.ca','6dbeeb3456db57818bb9b5a378d74e361d0898e822e07b4eb6df3080a9626a7b',3,0),(2,'ridiculus@hotmail.org','b5529ac3f4fcbcf6a672605595ef1eddca44d0ccb5147359c4e2218326139910',2,0),(3,'leocr@yahoo.org','52deb3fdc172e1910dbacce5cbab96090b253c03795644871b85d92d091e60de',2,0),(4,'magna@protonmail.couk','13d9048973b9768da89c5c6fefca88e1313e948a492a0aa5a47cae9329052b94',1,0),(5,'pretium@aol.edu','109f0ddf70137a22c8689c1c7102ae27d8caf360b45cc45d99eb8cb5cb65dcb9',1,0),(6,'dbabbs0@wufoo.com','75213f743c267951d40635fab8c259b53a29256349b17857ffd4dff0b0ae420e',1,0),(7,'dshyre1@nationalgeographic.com','21133b9d8922388ee03199e9b723c3b8b377ee169f65ca29b4534cd956f1d1a6',1,0),(8,'cnannoni2@wisc.edu','66d9bfa0899ef97b6bd94b20c6ef00c232fde96d38cdfb7e361c4b6d8f84c3a6',2,0),(9,'vliepmann3@freewebs.com','3313b47a1ea46b661d2d0f26871f9c27eed58bf3d10483578dd13b41ecba90e3',2,0),(10,'blorincz4@wired.com','79eeefdd14df2c699c492dd5fdb97c6700a8217c7f7afe6b35f93a00f9d71e15',2,0),(11,'shorsef5@acquirethisname.com','18e564a51eaa11b161d0f3ed73842e4ec704ac7154cca020058f9c5c395594af',1,0),(12,'wshorrock6@hubpages.com','19efadddb8844beaae352d831551d73117f3b784bc292306e1380cc58d670e76',1,0),(13,'sluis7@about.me','5c884af3538041f48521ad307c5b1ff951e1033fdcb6e0a4885613dec653768a',1,0),(14,'acotgrave8@techcrunch.com','76f71a72927a56689ac928d57b8ccc9da1c3c89194de0789fb395cf07760ef5f',2,0),(15,'fffff@gmail.com','043950549e7ce0e1e95856516147886ef890b7b8676842c9b4f8135a8d27c5f2',2,0),(16,'ccccc@gmail.com','cf0cdb7bb230598f0ea5f3d90da96c4f43baa28ddf60f6de8c1802b844d4826d',1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-14 13:15:41
